<?php

$config = new Config('localhost', 'database', 3306, Config::MYSQL_DRIVER, 'root', '123123', []);
$connection = new Connection($config);
$userProvider = new UserProvider($connection);

$usersIds = [];

/*
 * SQL иньекция
 */
foreach (explode(',', $_GET['user_ids']) as $userId) {
    if (\is_numeric($userId)) {
        $usersIds[] = (int) $userId;
    }
}

$usersData = $userProvider->usersList($usersIds);

foreach ($usersData as $key => $name) {
    echo "<a href=\"/show_user.php?id={$key}\">{$name}</a>";
}

class UserProvider
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * UserProvider constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $usersIds
     * @return array
     */
    public function usersList(array $usersIds): array
    {
        $ids = implode(', ', $usersIds);

        return $this
            ->connection
            ->connect()
            ->prepare('SELECT id, name FROM users WHERE id IN (:usersIds)')
            ->bindParam(':usersIds', $ids)
            ->execute()
            ->fetchAll(PDO::FETCH_KEY_PAIR)
        ;
    }
}

/**
 * Class Connection
 */
class Connection
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var PDO
     */
    private static $PDO;

    /**
     * Connection constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return PDO
     */
    public function connect(): PDO
    {
        if (null === self::$PDO) {
            self::$PDO = new \PDO(
                $this->config->getDsn(),
                $this->config->getUsername(),
                $this->config->getPassowrd(),
                $this->config->getOptions()
            );
        }

        return self::$PDO;
    }
}

/**
 * Class Config
 */
class Config
{
    public const MYSQL_DRIVER = 'mysql';

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $dbName;

    /**
     * @var int
     */
    private $port;

    /**
     * @var string
     */
    private $driver;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var array
     */
    private $options;

    /**
     * Config constructor.
     * @param string $host
     * @param string $dbName
     * @param int $port
     * @param string $driver
     * @param string $username
     * @param string $password
     * @param array $options
     */
    public function __construct(string $host, string $dbName, int $port, string $driver, string $username, string $password, array $options)
    {
        $this->host = $host;
        $this->dbName = $dbName;
        $this->port = $port;
        $this->driver = $driver;
        $this->username = $username;
        $this->password = $password;
        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getDsn(): string
    {
        return $this->driver . ':host=' . $this->host . ';port=' . $this->port . ';dbname=' . $this->dbName;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}
